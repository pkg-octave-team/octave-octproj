/* -*- coding: utf-8 -*- */
/**
\ingroup octproj
@{
\file geodgeoc.h
\brief Function declaration for geod2geoc and geoc2geod functions.
\author José Luis García Pallero, jgpallero@gmail.com
\date 22-02-2020
\section License License
This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
#ifndef _GEODGEOC_H_
#define _GEODGEOC_H_
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
#ifndef M_PI
#define M_PI (3.141592653589793)
#endif
/******************************************************************************/
/******************************************************************************/
/**
\brief First vertical radius of curvature.
\param[in,out] lat Geodetic latitude, in radians.
\param[in] a Semi-major axis of the ellipsoid, in meters.
\param[in] e2 Squared first eccentricity of the ellipsoid.
\return First vertical radius of curvature.
\date 22-02-2020: Function creation.
*/
double octproj_rpm(const double lat,
                   const double a,
                   const double e2);
/******************************************************************************/
/******************************************************************************/
/**
\brief Transform from geodetic to geocentric coordinates.
\param[in,out] u Array containing the geodetic longitude, in radians. On output,
               it contains the X geocentric coordinate, in meters.
\param[in,out] v Array containing the geodetic latitude, in radians. On output,
               it contains the Y geocentric coordinate, in meters.
\param[in,out] w Array containing the ellipsoidal height, in meters. On output,
               it contains the Z geocentric coordinate, in meters.
\param[in] nElem Number of elements in \em u, \em v, and \em w arrays.
\param[in] incElem Number of positions between each element in the arrays \em u,
           \em v, and \em w (must be a positive number).
\param[in] a Semi-major axis of the ellipsoid, in meters.
\param[in] e2 Squared first eccentricity of the ellipsoid.
\date 22-02-2020: Function creation.
*/
void octproj_geod2geoc(double* u,
                       double* v,
                       double* w,
                       const size_t nElem,
                       const size_t incElem,
                       const double a,
                       const double e2);
/******************************************************************************/
/******************************************************************************/
/**
\brief Transform from geocentric to geodetic coordinates.
\param[in,out] u Array containing the X geocentric coordinate, in meters. On
                 output, it contains the geodetic longitude, in radians and in
                 (-pi,pi] domain.
\param[in,out] v Array containing the Y geocentric coordinate, in meters. On
                 output, it contains the geodetic latitude, in radians and in
                 (-pi/2,pi/2) domain.
\param[in,out] w Array containing the Z geocentric coordinate, in meters. On
                 output, it contains the ellipsoidal height, in meters.
\param[in] nElem Number of elements in \em u, \em v, and \em w arrays.
\param[in] incElem Number of positions between each element in the arrays \em u,
           \em v, and \em w (must be a positive number).
\param[in] a Semi-major axis of the ellipsoid, in meters.
\param[in] e2 Squared first eccentricity of the ellipsoid.
\date 22-02-2020: Function creation.
*/
void octproj_geoc2geod(double* u,
                       double* v,
                       double* w,
                       const size_t nElem,
                       const size_t incElem,
                       const double a,
                       const double e2);
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/** @} */
