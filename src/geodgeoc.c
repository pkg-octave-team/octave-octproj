/* -*- coding: utf-8 -*- */
/**
\ingroup octproj
@{
\file geodgeoc.c
\brief Function definition for geod2geoc and geoc2geod functions.
\author José Luis García Pallero, jgpallero@gmail.com
\date 22-02-2020
\section License License
This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
#include"geodgeoc.h"
/******************************************************************************/
/******************************************************************************/
double octproj_rpm(const double lat,
                   const double a,
                   const double e2)
{
    return a/sqrt(1.0-e2*sin(lat)*sin(lat));
}
/******************************************************************************/
/******************************************************************************/
void octproj_geod2geoc(double* u,
                       double* v,
                       double* w,
                       const size_t nElem,
                       const size_t incElem,
                       const double a,
                       const double e2)
{
    //index for loop
    size_t i=0;
    //position
    size_t pos=0;
    //transformed values
    double x=0.0,y=0.0,z=0.0;
    //auxiliary value
    double nu=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //loop over the points
    for(i=0;i<nElem;i++)
    {
        //position in the arrays
        pos = i*incElem;
        //transformation
        nu = octproj_rpm(v[pos],a,e2);
        x = (nu+w[pos])*cos(v[pos])*cos(u[pos]);
        y = (nu+w[pos])*cos(v[pos])*sin(u[pos]);
        z = (nu*(1.0-e2)+w[pos])*sin(v[pos]);
        //assignt to output
        u[pos] = x;
        v[pos] = y;
        w[pos] = z;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //exit
    return;
}
/******************************************************************************/
/******************************************************************************/
void octproj_geoc2geod(double* u,
                       double* v,
                       double* w,
                       const size_t nElem,
                       const size_t incElem,
                       const double a,
                       const double e2)
{
    //index for loop
    size_t i=0;
    //position
    size_t pos=0;
    //transformed values
    double lon=0.0,lat=0.0,h=0.0;
    //tolerances
    double tol=1.0e-6,tolLat=tol/a;
    //auxiliary values
    double distXY=0.0,nu=0.0,lat0=0.0,h0=0.0,difLat=0.0,difH=0.0,aux=0.0;
    int less=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //loop over the points
    for(i=0;i<nElem;i++)
    {
        //position in the arrays
        pos = i*incElem;
        //geodetic longitude is computed in one step
        lon = atan2(v[pos],u[pos]);
        //geocentric vector in XY plane
        distXY = sqrt(u[pos]*u[pos]+v[pos]*v[pos]);
        //first approximation to latitude
        lat0 = atan(w[pos]/((1.0-e2)*distXY));
        //first vertical curvature radius
        nu = octproj_rpm(lat0,a,e2);
        //check if lat0 is less than PI/4
        less = fabs(lat0)<(M_PI/4.0);
        //first approximation to ellipsoidal height
        h0 = less ? distXY/cos(lat0)-nu : w[pos]/sin(lat0)-(1.0-e2)*nu;
        //if we work on the sphere, the coordinates are computed
        if(e2==0.0)
        {
            u[pos] = lon;
            v[pos] = lat0;
            w[pos] = h0;
            continue;
        }
        //initial differences
        difLat = 2.0*tolLat;
        difH = 2.0*tol;
        //loop while differences are greater than tolerances
        while((difLat>tolLat)||(difH>tol))
        {
            //auxiliary value
            aux = 1.0-e2*nu/(nu+h0);
            //next approximation to latitude
            lat = atan(w[pos]/(distXY*aux));
            //first vertical curvature radius
            nu = octproj_rpm(lat,a,e2);
            //ellipsoidal height
            h = less ? distXY/cos(lat)-nu : w[pos]/sin(lat)-(1.0-e2)*nu;
            //differences absolute values
            difLat = fabs(lat-lat0);
            difH = fabs(h-h0);
            //updated values
            lat0 = lat;
            h0 = h;
        }
        //assign to output
        u[pos] = lon;
        v[pos] = lat;
        w[pos] = h;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //exit
    return;
}
/******************************************************************************/
/******************************************************************************/
/** @} */
